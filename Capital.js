const {City} = require('./City');

class Capital extends City{
    /**
     * @param  {string} name Name of city
     */
    constructor(name){
        super(name);
        this.airportLocation = '';
    }
    /**
     * @param  {string} location Location of airport
     */
    setAirport(location){
        this.airportLocation = location;
    }
}

module.exports = {Capital};