const {Weather} = require('./Weather');

class City{
    /**
     * @param {string} name Name of city
     */
    constructor(name) {
        this.name = name;
        this.weather = new Weather();
    }

    async setWeather(){
        await this.weather.setWeather(this.name);
    }

    async setForecast(){
        await this.weather.setForecast(this.name);
    }

    async setWeatherAndForecast(){
        await this.weather.setWeatherAndForecast(this.name);
    }
}

module.exports = {City};