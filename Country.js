const {City} = require('./City');

class Country{
    /**
     * @param  {string} name Name of Country
     * @param  {string[]} cities Array of cities
     */
    constructor(name, cities = []) {
        this.name = name;
        this.cities = cities;
    }

    /**
     * @param  {string} name Name of city
     */
    async addCity(name){
        await this.cities.push(new City(name));
    }

    /**
     * @param  {string} name Name of city
     */
    removeCity(name) {
        const indexToRemove = this.cities.findIndex(city => city.name === name);
        this.cities.splice(indexToRemove, 1);
    }

    async setWeatherForAll() {
        for (const city of this.cities) {
            await city.setWeather();
          }
    }

    showWeather() {
        this.cities.forEach( city => {
            console.log(`${city.name}: `, city.weather)
        });
    }

    sortCitiesByTemperature() {
        this.cities.sort( (a,b) => this.getTemperature(b) - this.getTemperature(a));
    }

    /**
     * @param  {string} city Name of city
     */
    getTemperature (city){
        const { temperature } = city.weather;
        return +temperature.slice(0,temperature.indexOf(" "));
    }
}

module.exports = {Country}