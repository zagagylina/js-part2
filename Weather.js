const axios = require('axios');

const weatherUrl = 'https://goweather.herokuapp.com/weather/';
const today = new Date();

class Weather{
    /**
     * Describe attributes: temperature, wind, date
     */
    constructor() {
        this.temperature = '';
        this.wind = '';
        this.date = today.toLocaleDateString();
        this.forecast = [];
    }

    /**
     * @param  {string} cityName
     */
    async setWeather(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.temperature = response.data.temperature;
            this.wind = response.data.wind;
        }).catch((error) => {return error.message});
    }
    
    /**
     * @param  {string} cityName Name of city
     */
    async setForecast(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.forecast = response.data.forecast;
        }).catch((error) => {return error.message});
    }
    
    /**
     * @param  {string} cityName Name of city
     */
    async setWeatherAndForecast(cityName){
        await this.setWeather(cityName);
        await this.setForecast(cityName);
    }
};

module.exports = {Weather};