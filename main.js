const {City} = require('./City');
// const {Capital} = require('./Capital');
const {Country} = require('./Country');

let city1 = new City('Kharkiv');
let city2 = new City('Lviv');

// city1.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
// city2.setWeather().then(() => {console.log(city2)}).catch((error) => {console.log(error)})

// let capital = new Capital('Kyiv')
// capital.setAirport('Boryspil')
// capital.setWeather().then(() => {console.log(capital)}).catch((error) => {console.log(error)})

const newCities = [
    'Dnipro',
    'Odessa',
    'Rivne',
    'Poltava',
    'Uzhgorod',
    'Zaporizhzhia',
    'Kherson',
    'Chernihiv',
];

const runMain = async () =>  {
    // create country Ukraine with several cities
    let ukraine = new Country('Ukraine', [city1, city2]);
    // add ten more cities
    newCities.forEach( cityName => ukraine.addCity(cityName));

    // set weather for all
    await ukraine.setWeatherForAll();

    // remove one city
    ukraine.removeCity('Kharkiv');

    // sort by temperature
    ukraine.sortCitiesByTemperature();

    // show result 
    ukraine.showWeather();
};

runMain();